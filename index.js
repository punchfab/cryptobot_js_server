process.env.TZ = 'America/Bogota';

// key aaN6u4MU5SM61O0cCMTSV82GYvRL8iyZyoHGyLLuqrxKFE8I0Z8AecfykuF0QsBd
// secret gYtkcwiePMxFRZ9tNw0PjQTd5ZfjMPoVfc0uuiwmAx0KahGS5DVcsOt3Aj17PcBP

/*
Aplicacion que haga ordenes automaticas precedida de una orden exitosa
indicaciones de la aplicacion:

- precio actual, cambio de las ultimas 24H
- precio de entrada: se especifica manualmente (se omitira si ya se ingreso)
- stop loss: se especifica manualmente; este se ejecutara una vez que el ingreso sea exitoso 
	(si hay una transaccion en curso se habilitara la actualizacion)
- profit(precio de salida): se especifica manualmente; se puede ejecutar junto con el stop loss como operacion "OCO"
	(si hay una transaccion en curso se habilitara la actualizacion)
- el precio * la cantidad deben ser igual o mayor al minimo establecido
- porcentaje de ganancia/perdida en cada elemento
- algunos pares no aceptan cualquier precio algunos deben ser redondeados segun el balance
- cantidad automatica: balance total / precio = resultado < balance
- mostrar mensaje de error si la orden no se ejecuta
- la actualizacion de los elementos indica que las ordenes establecidas se eliminaran y se crean nuevas ordenes

Minimun
-BTC: 0.0001
-BNB: 0.1
-ETH: 0.01
-USDT: 10
-TRON: 100
-XRP: 10
*/
require('./config/database');
require('dotenv').config();
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const Binance = require('node-binance-api');
const bodyParser = require("body-parser");
const verifyToken = require('./functions/verifyToken')
const jwt = require('jsonwebtoken');
const fbsadmin = require("firebase-admin");


// Initializations
var serviceAccount = require("./config/cryptobot-client-firebase-adminsdk.json");

fbsadmin.initializeApp({
  credential: fbsadmin.credential.cert(serviceAccount),
  databaseURL: "https://cryptobot-client.firebaseio.com"
});
console.log('api rest');
console.log(process.env.APIKEY);
const binance = new Binance().options({APIKEY:process.env.APIKEY_NEW, APISECRET:process.env.APISECRET_NEW});
console.log('futures');
const fbinance = new Binance().options({
  APIKEY:process.env.APIKEY_NEW, 
  APISECRET:process.env.APISECRET_NEW,
  urls: {
    stream: 'wss://fstream.binance.com/ws/', 
    combineStream: 'wss://fstream.binance.com/ws/'
  }
});
const sleep = (seconds) => new Promise(resolve => setTimeout(resolve, seconds * 1000));
const Dsymbol = require('./models/symbol');
const Trade = require('./models/trade');
const Balance = require('./models/balance');
const User = require('./models/user');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/*(async () => {
	let ticker = await binance.prices();
  console.log(Object.keys(ticker));
})();*/

/*var quantity = 4.7, price = 0.02194;
binance.buy("ALGOBNB", quantity, price, {icebergQty: "4.7"}, (error, response) => {
  console.log("Limit Buy response", response);
  console.log("order id: " + response.orderId);
  if (error) console.log(error.body);
});*/
var gbPrev = [];
var publicDir = (__dirname+'/public/');
app.use(express.static(publicDir));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/js', express.static(__dirname + '/node_modules/popper.js/dist'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.set('view engine', 'pug');
app.get('/', async (req, res) => {
	res.send('Server connected');
});

app.get('/web/futures/:fsymbol', async (req, res) => {
  const {fsymbol} = req.params;
  let posRisk = await binance.futuresPositionRisk();
  let daily = await binance.futuresDaily(fsymbol);
  let fsymbols = (await binance.futuresExchangeInfo())["symbols"];
  let arrfs = fsymbols.findIndex( ({ symbol }) => symbol === fsymbol );
  let balance = (await binance.futuresBalance())[0].balance;
  var pRiskIndex = posRisk.findIndex( ({ symbol }) => symbol === symbol );
  let leverage = posRisk[pRiskIndex].leverage;
  let maxSize = balance * leverage;
  console.log((await binance.futuresUserTrades( "XTZUSDT" , {fromId: 20670898}))[0].price)
  // console.info('positionrisk', );
  // console.log(fsymbols[arrfs]);
  // console.info( await binance.futuresLeverageBracket( "LINKUSDT", {timestamp:Date.now()} ) );
  // console.info( (await binance.futuresBalance())[0].balance );
  // console.info( await binance.futuresAccount() );
  // console.info( await binance.futuresLeverageBracket( "LINKUSDT", {timestamp:Date.now()} ) );
  // console.info('openOrders', await binance.futuresOpenOrders( "XTZUSDT" ) );
  console.info('quote', await binance.futuresQuote(fsymbol) );
  console.info('depth', await binance.futuresDepth(fsymbol) );
  console.info('price', daily );
  // var t0 = await Trade.findOne({status: 'COMPLETED'});
  // sendNotification(`Close Operation ${t0.symbol} - ${(t0.positionSide).toUpperCase()}`, `${t0.symbol} Entry ${t0.price.entry} | Exit ${t0.price.exit}`);
  res.render('futures',{
    symbol: fsymbol,
    lastPrice: daily.lastPrice, 
    percentChange: daily.priceChangePercent, 
    pricePrecision: fsymbols[arrfs]["pricePrecision"],
    quantityPrecision: fsymbols[arrfs]["quantityPrecision"],
    filters: fsymbols[arrfs]["filters"],
    leverage,
    balance,
    maxSize
  });
});

app.post('/web/futures', async (req, res) => {
  var { symbol, positionside, openprice, openamount, closeprice, closestop, closelimit, closeamount } = req.body;
  var fsymbol = symbol;
  var activeBuy = true;
  var activeSell = true;
  var type = 2;
  // console.log(req.body);
  var fsymbols = (await binance.futuresExchangeInfo())["symbols"];
  var arrfs = fsymbols.findIndex( ({ symbol }) => symbol === fsymbol );
  // console.info(fsymbols[arrfs]);

  var buyprice = positionside == "LONG" ? openprice : closeprice;
  
  var buystop = positionside == "SHORT" ? closestop : undefined;
  var buystoplimit = positionside == "SHORT" ? closelimit : undefined;

  var sellstop = positionside == "LONG" ? closestop : undefined;
  var sellstoplimit = positionside == "LONG" ? closelimit : undefined;
  
  var sellprice = positionside == "LONG" ? closeprice : openprice;
  var buyamount = positionside == "LONG" ? openamount : closeprice;
  var sellamount = positionside == "LONG" ? closeamount : openamount;
  let fbalance = await fbinance.futuresBalance();
  var pRiskIndex = (await fbinance.futuresPositionRisk()).findIndex( ({ symbol }) => symbol === fsymbol );
  let leverage = (await fbinance.futuresPositionRisk())[pRiskIndex].leverage;

  let result_trade = {
    'symbol': symbol,
    'market' : 'futures',
    'positionSide' : positionside.toLowerCase(),
    'leverage' : leverage,
    'quantity' : openamount,
    'balance': { 'initial': fbalance[0].balance },
    'price': {
      'entry': openprice,
      'exit': closeprice
    },
    'buy': {
      'type' : type,
      'active': activeBuy,
      'limit': buyprice,
      'stop' : buystop,
      'stop_limit' : buystoplimit,
      'amount': buyamount,
    },
    'sell': {
      'type': type,
      'active': activeSell,
      'limit': sellprice,
      'stop': sellstop,
      'stop_limit': sellstoplimit,
      'amount': sellamount
    }
  };

  var sideorder = positionside == "LONG" ? "BUY" : "SELL";
  var typeorder = "LIMIT";
  Object.assign(result_trade, {'status': 'PENDING'});
  const newTrade = new Trade(result_trade);
  let fTrade = await newTrade.save();
  // console.info( await binance.futuresOrderStatus( "XTZUSDT", {orderId: "268613610"} ) );
  console.log("Orden creada");
  let orderfuture = await binance.futuresOrder( sideorder, symbol, openamount, openprice, {positionSide: positionside} );
  console.info(orderfuture);
  if (positionside == "LONG") {
    // BUY
    fTrade.buy.order = {id: orderfuture.orderId, status: orderfuture.status};
  } else {
    // SELL
    fTrade.sell.order = {id: orderfuture.orderId, status: orderfuture.status};
  }
  await fTrade.save();
  // console.info(await binance.futuresOrder( 'BUY', 'BTCUSDT', 0.001, 3000, {positionSide: 'LONG', type: 'TAKE_PROFIT', stopPrice: 3100} ));
  // LIMIT STOP MARKET STOP_MARKET TAKE_PROFIT TAKE_PROFIT_MARKET
  // console.info(await binance.futuresOrder( 'SELL', 'BTCUSDT', 0.001, 11000, {positionSide: 'SHORT'} ));
  //long
    // buy
    // sell
  //short
    // sell
    // buy
  // console.info( await binance.futuresBuy( 'BTCUSDT', 0.1, 3222, {positionSide:'LONG'} ) );
  // console.info( await binance.futuresSell( 'BTCUSDT', 0.001, 11111.89, {positionSide: 'SHORT'} ) );
  res.send(symbol);

});

app.get('/digs/:symbol', verifyToken, async (req, res) => {
  const {symbol} = req.params;
  let coin = await Dsymbol.findOne({pair: symbol, active:true});
  console.log("digs...");
  res.end(JSON.stringify(coin));
});

app.get('/prev', verifyToken, async (req, res) => {
  res.end(JSON.stringify(gbPrev));
  console.log("prev...");
});

/*app.get('/open/:symbol/:orderId', async (req, res) => {
  const {symbol, orderId} = req.params;

  try {
    let openOrders = await binance.openOrders(symbol);
    let orderStatus = await binance.orderStatus(symbol, orderId);

    let orders = { open: openOrders, status: orderStatus };
    res.end(JSON.stringify(orders));
    console.log("orders: ", orders);
  } catch(e) {
    var error = e;
    if (e.body) error = e.body;
    res.send("error producido");
    console.error("open error: ",error);
  }

});*/

app.get('/order_cancel/:symbol', verifyToken, async (req, res) => {
  const {symbol} = req.params;
  // console.log("order cancel...");
  const trade = await Trade.findOne({symbol: symbol, status: 'PENDING'});
  let orderId = null;
  if (typeof trade.buy.order.id !== 'undefined') {
    if (trade.buy.order.status == 'NEW') orderId = trade.buy.order.id;
  } else if (typeof trade.sell.order.id !== 'undefined' && trade.sell.type !== '2') {
    if (trade.sell.order.status == 'NEW') orderId = trade.sell.order.id;
  } else if (typeof trade.sell.order.oco0.id !== 'undefined') {
    if (trade.sell.order.oco0.status == 'NEW' && trade.sell.order.oco1.status == 'NEW') orderId = trade.sell.order.oco0.id;
  }
  trade.status = 'CANCELED';
  await trade.save();

  if (orderId !== null) {
    binance.cancel(trade.symbol, orderId, (error, response, symbol) => {
      io.emit(`${symbol}_status`, {status:'CANCELED'});
      console.log(symbol+" cancel response:", response);
    });
  }

  await emitUpdateTrades();

  res.end(JSON.stringify('OK'));
});


app.get('/dsymbol', verifyToken, async (req, res) => {
    /*const newSymbol = new Dsymbol({ pair: 'LTCBNB', digits: 5 });
    await newSymbol.save();*/
    // console.log(newSymbol);
    // let newSymbol;

    /*binance.prevDay(false, (error, coins) => {
      coins.forEach(function(obj, index) {
        updateSymbols(obj);
      });
    });*/
    console.log("dsymbol...");
    res.send("Ok");
});

app.get('/balance/:symbol', verifyToken, async (req, res) => {
  const {symbol} = req.params;
  console.log("balance...");
  const coins = ["BTC", "BNB", "ETH", "USDT"];
  let coin0;
  let coin1;
  var coinBalance = [];
  var i = 0
  let search = symbol.search(coins[0]);

  do {
    search = symbol.search(coins[i]);
    console.log('search: ',search);
    if (search == 0) search = -1;

    coin0 = symbol.split(coins[i])[0];
    // console.log('coin0: ',coin0);
    coin1 = coins[i];
    // console.log('coin1: ',coin1);
    i++;
  } while (search == -1 && i<4);
  let assets = [coin0,coin1];

  console.log(`${coin0} - ${coin1}`);
  for (let asset of assets) {
    let balance = await Balance.findOne({asset}).select('-_id asset available onOrder');
    coinBalance.push(balance);
  }

  res.end(JSON.stringify(coinBalance));

  /*binance.balance((error, balances) => {
    if ( error ) return console.error('balance:', error.body);
    try {
      balances[coin0].coin = coin0;
      balances[coin1].coin = coin1;
      coinBalance.push(balances[coin0]);
      coinBalance.push(balances[coin1]);
      // coinBalance.coin = coin;
      res.end(JSON.stringify(coinBalance));
    } catch (e) {
      console.error(`error con la peticion ${e}`);
      res.end(JSON.stringify({error:"unknown"}));
    }*/
    // console.log("balances()", balances);
    // console.log(`${coin} balance:`, balances[coin].available);
    
  // });
});

app.get('/trades', verifyToken, async (req, res) => {
  var trades = await Trade.find().or([{status: 'PENDING'}, {status: 'COMPLETED'}]).sort({ _id:-1 });
  console.log("balance....");
  res.end(JSON.stringify(trades));
});

app.get('/trade/:symbol', verifyToken, async (req, res) => {
  const {symbol} = req.params;
  // console.log("trade....", symbol);
  var trade = await Trade.findOne({ symbol: symbol, status: 'PENDING' });
  res.end(JSON.stringify(trade));
})

  /*
  status:
  -new
  -pending
  -complete
  */
app.post('/set_order', verifyToken, async (req, res) => {
  console.log("set order....");
  var symbol = req.body.symbol;
  var type = req.body.type;
  var activeBuy = req.body.buy_active == 'true';
  var activeSell = req.body.sell_active == 'true';
  var buy_limit = req.body.buy_limit;
  var buy_amount = req.body.buy_amount;
  var sell_limit = req.body.sell_limit;
  var sell_stop = req.body.sell_stop;
  var sell_stop_limit = req.body.sell_stop_limit;
  var sell_amount = req.body.sell_amount;

  var msg = `symbol: ${symbol}\n`;
  msg += `type: ${type}\n`;
  msg += `activeBuy: ${activeBuy}\n`;
  msg += `activeSell: ${activeSell}\n`;
  msg += `buy_limit: ${buy_limit}\n`;
  msg += `buy_amount: ${buy_amount}\n`;
  msg += `sell_limit: ${sell_limit}\n`;
  msg += `sell_stop: ${sell_stop}\n`;
  msg += `sell_stop_limit: ${sell_stop_limit}\n`;
  msg += `sell_amount: ${sell_amount}`;

  var typeOrder = {type: 'LIMIT'};
  var priceLimit = (type == '1') ? sell_stop_limit : sell_limit;
  switch (type) {
    case '1':
      typeOrder = {type: 'STOP_LOSS_LIMIT', stopPrice: sell_stop};
      break;
    case '2':
      typeOrder = {type: 'OCO', stopLimitPrice: sell_stop_limit, stopPrice: sell_stop};
      break;
  }

  let result_trade = {
    'symbol': symbol,
    'price': {
      'entry': buy_limit,
      'exit': priceLimit
    },
    'buy': {
      'active': activeBuy,
      'limit': buy_limit,
      'amount': buy_amount
    },
    'sell': {
      'type': type,
      'active': activeSell,
      'limit': sell_limit,
      'stop': sell_stop,
      'stop_limit': sell_stop_limit,
      'amount': sell_amount
    }
  };
  
  // let newTrade;
  let error = false;
  let eMsg = "Error del servidor";
  let trades = await Trade.findOne({'symbol': symbol, 'status': 'PENDING'});
  if (activeBuy) {
    // si la compra esta activa
    if (trades) {
      // hay una transaccion pendiente
      try{
        // cancelar orden existente
        if (typeof trades.buy.order.id !== 'undefined' && trades.buy.order.status == 'NEW') {
          let orderCancel = await binance.cancel(symbol, trades.buy.order.id);
          console.log(symbol+" cancel response:", orderCancel);
        }
        // colocar nueva orden
        trades.price = result_trade.price;
        trades.buy = result_trade.buy;
        trades.sell = result_trade.sell;
        // trades.buy.order = {id: setOrder.orderId, status: setOrder.status};
        trades.status = 'PENDING';
        await trades.save();
        let setOrder = await binance.buy(symbol, buy_amount, buy_limit, {type:'LIMIT'});
        console.log(setOrder);
        io.emit(`${symbol}_status`, {status:'PENDING'});
      } catch(e) {
        error = true;
        if (e.body) eMsg = (JSON.parse(e.body)).msg;
        console.log(e.body);
      }
      console.log('trade exist');
    } else {
      // nueva transaccion
      console.log('trade not found');
      try {
        // result_trade.buy.order = {'id': setOrder.orderId, 'status': setOrder.status};
        Object.assign(result_trade, {'status': 'PENDING'});
        const newTrade = new Trade(result_trade);
        await newTrade.save();
        let setOrder = await binance.buy(symbol, buy_amount, buy_limit, {type:'LIMIT'});
        io.emit(`${symbol}_status`, {status:'PENDING'});
        console.log("new trade:", newTrade);
      } catch (e) {
        error = true;
        if (e.body) eMsg = (JSON.parse(e.body)).msg;
        console.log(e.body);
      }
    }
  } else if (activeSell) {
    // venta pendiente para actualizar
    if (trades) {
        try {
          // cancela orden existente
          if ((typeof trades.sell.order.id !== 'undefined' && trades.sell.order.status == 'NEW') || 
            (typeof trades.sell.order.oco0.id !== 'undefined' && trades.sell.order.oco0.status == 'NEW' && trades.sell.order.oco1.status == 'NEW')) {
            var orderId = (typeof trades.sell.order.id !== 'undefined') ? trades.sell.order.id : trades.sell.order.oco0.id;
            let orderCancel = await binance.cancel(symbol, orderId);
            console.log(symbol+" cancel response:", orderCancel);
          }
          // colocar nueva orden de venta
          trades.price = result_trade.price;
          trades.sell = result_trade.sell;
          /*if (type == '2') {
            trades.sell.order = {
              oco0: {id: setOrder.orderReports[0].orderId, status: setOrder.orderReports[0].status},
              oco1: {id: setOrder.orderReports[1].orderId, status: setOrder.orderReports[1].status}
            }
          } else {
            setOrder = await binance.orderStatus(symbol, setOrder.orderId);
            trades.sell.order = {id: setOrder.orderId, status: setOrder.status};
          }*/
          await trades.save();
          let setOrder = await binance.sell(symbol, sell_amount, priceLimit, typeOrder);
          if (type == '2') {
            trades.sell.order = {
              oco0: {id: setOrder.orderReports[0].orderId, status: setOrder.orderReports[0].status},
              oco1: {id: setOrder.orderReports[1].orderId, status: setOrder.orderReports[1].status}
            }
          }
          await trades.save();
          console.log('orden de venta: ', setOrder);
          io.emit(`${symbol}_status`, {status:'PENDING'});
        } catch (e) {
          error = true;
          if (e.body) eMsg = (JSON.parse(e.body)).msg;
          console.error(e.body);
        }
    } else {
      // nueva venta unica
      try {
        /* else {
          setOrder = await binance.orderStatus(symbol, setOrder.orderId);
          result_trade.sell.order = {'id': setOrder.orderId, 'status': setOrder.status};
        }*/
        Object.assign(result_trade, {'status': 'PENDING'});
        const newTradeSell = new Trade(result_trade);
        let nts = await newTradeSell.save();
        let setOrder = await binance.sell(symbol, sell_amount, priceLimit, typeOrder);
        let tr = await Trade.findOne({ _id:nts._id });
        if (type == '2') {
          tr.sell.order = {
            oco0: {id: setOrder.orderReports[0].orderId, status: setOrder.orderReports[0].status},
            oco1: {id: setOrder.orderReports[1].orderId, status: setOrder.orderReports[1].status}
          }
        };
        console.log(setOrder);
        console.log("new trade sell:", newTradeSell);
        io.emit(`${symbol}_status`, {status:'PENDING'});
      } catch (e) {
        error = true;
        if (e.body) eMsg = (JSON.parse(e.body)).msg;
        console.error(e.body);
      }
    }
  }

  if (error) res.end(JSON.stringify(eMsg));
  else {
    res.end(JSON.stringify('success'));
    await emitUpdateTrades();
  }
});

app.get('/signup', async (req, res) => {
    try {
        // Receiving Data
        const { username, email, password } = req.body;
        // Creating a new User
        const user = new User({
            username: "cpbotn4",
            email: "sales.gameplux@gmail.com",
            password: "gplX8812fhi5"
        });
        user.password = await user.encryptPassword(user.password);
        await user.save();
        res.json({ auth: true });

    } catch (e) {
        console.log(e)
        res.status(500).send('There was a problem registering your user');
    }
});

app.post('/login', async (req, res) => {
    const user = await User.findOne({email: req.body.email})
    if(!user) {
        return res.status(404).send("The email doesn't exists")
    }
    const validPassword = await user.comparePassword(req.body.password, user.password);
    if (!validPassword) {
        return res.status(401).send({auth: false, token: null});
    }
    const token = jwt.sign({id: user._id}, process.env.SECRET, {
        expiresIn: 60 * 60 * 24
    });
    // console.log(token);
    res.status(200).json(JSON.stringify({auth: true, token}));
});

app.get('/me', verifyToken, async (req, res) => {
  console.log("set order....");
  // res.status(200).send(decoded);
  // Search the Info base on the ID
  // const user = await User.findById(decoded.id, { password: 0});
  /*const user = await User.findById(req.userId, { password: 0});
  if (!user) {
      return res.status(404).send("No user found.");
  }*/
  res.status(200).send("correct auth");
});

app.get('/order', async (req, res) => {
  /*const tr = await Trade.findOne({symbol:'LTCBNB', status: 'PENDING'});
  if (tr) console.log(tr.buy.order.status);
  else console.log('no existe');*/
  // let b = typeof tr.buy.order.id == 'undefined'
  /*try {
    let buy = await binance.sell('BNBBTC', '0.10', '0.0011033', {type: 'STOP_LOSS_LIMIT', stopPrice: '0.0011032'});
    console.log(buy);
    const l = await binance.orderStatus('BNBBTC', buy.orderId);
    console.log(l);
  } catch (e) {
    console.log(e.body);
  }*/
  let endpoints = binance.websockets.subscriptions();
  console.log('endpoints: ', Object.keys(endpoints).length);
  for ( let endpoint in endpoints ) {
    console.log(endpoint);
    //binance.websockets.terminate(endpoint);
  }

  console.info( await fbinance.futuresPositionMode('true') );
  res.send("OK");
});

let sub = binance.websockets.prevDay(false, (error, response) => {
  // console.info(response);
  var rsPrev = (gbPrev.length > 0) ? gbPrev.findIndex( ({ symbol }) => symbol === response.symbol ) : -1;
  var {search} = getCoin(response.symbol);
  if ( search !== -1 && search !== 0 && parseFloat(response.quoteVolume) !== 0 && parseFloat(response.close) !== 0 && parseFloat(response.percentChange) !== 0) {
    if ( rsPrev == -1 ) gbPrev.push(response);
    else gbPrev[rsPrev] = response;
  }
  (async () => {
    await updateSymbols(response);
  })();
});
console.info('subs:',sub);
let gArrayPrev = {
  'BTC': {'1m':[],'3m':[],'5m':[],'15m':[],'30m':[],'1h':[],'2h':[],'4h':[],'6h':[],'8h':[],'12h':[],'1d':[],'3d':[],'1w':[],'1M':[]},
  'ETH': {'1m':[],'3m':[],'5m':[],'15m':[],'30m':[],'1h':[],'2h':[],'4h':[],'6h':[],'8h':[],'12h':[],'1d':[],'3d':[],'1w':[],'1M':[]},
  'USDT': {'1m':[],'3m':[],'5m':[],'15m':[],'30m':[],'1h':[],'2h':[],'4h':[],'6h':[],'8h':[],'12h':[],'1d':[],'3d':[],'1w':[],'1M':[]},
  'BNB': {'1m':[],'3m':[],'5m':[],'15m':[],'30m':[],'1h':[],'2h':[],'4h':[],'6h':[],'8h':[],'12h':[],'1d':[],'3d':[],'1w':[],'1M':[]}
};
let kG = Object.keys(gArrayPrev);
let pD = Object.keys(gArrayPrev.BTC);
let p = kG.findIndex((element) => element === 'BTC');
var ssrt = false;
var sio = io;
var optionsSort = {coin:'',period:'',percent:'',sort:'',currentArray:[],pairsArray:[]};
// let pD = ['1m','3m','5m','15m','30m','1h','2h','4h','6h','8h','12h','1d','3d','1w','1M'];
(async () => {
  var dbSymbols = await Dsymbol.find({active: true});
  var arraySymbols = [];
  for (let obj of dbSymbols) {
    let symbol = obj.pair;
    var {search} = getCoin(symbol);
    if ( search !== -1 && search !== 0 ) arraySymbols.push(symbol);
  }
  for (let period of pD) {
    // var l = ''
    let subw = binance.websockets.candlesticks(arraySymbols, period, (candlesticks) => {
      let objTick = simpleTick(candlesticks);
      let {coin,search} = getCoin(objTick.symbol);
      // console.log(objTick);
      // console.info(symbol+" "+period+" candlestick update");
      let resultIndex = (gArrayPrev[coin][period].length > 0) ? gArrayPrev[coin][period].findIndex( ({ symbol }) => symbol === objTick.symbol ) : -1;
      if (resultIndex == -1 && search !== -1 && search !== 0) {
        gArrayPrev[coin][period].push(objTick);
        // console.log(`${coin} ${period} ${gArrayPrev[coin][period].length}`);
        /*console.info('currentArray: ',optionsSort.currentArray.length);
        console.info('pairsArray: ',optionsSort.pairsArray.length);
        console.info('coin:',optionsSort.coin);
        console.info('period:',optionsSort.period);
        console.info('percent:',optionsSort.percent);
        console.info('sort:',optionsSort.sort);
        console.info('ssrt', ssrt);
        console.info("");*/
      }else if(resultIndex !== -1 && search !== -1 && search !== 0) {
        gArrayPrev[coin][period][resultIndex] = objTick;
        if (ssrt) {
          if (optionsSort.period == period && optionsSort.coin == coin /*&& optionsSort.pairsArray.length > 0*/) {
            let array = gArrayPrev[coin][period];
            for (let obj of optionsSort.pairsArray) {
              let ri = array.findIndex( ({ symbol }) => symbol === obj.symbol );
              if (ri == -1 && ssrt) array.push(obj);
            }
            console.log(gArrayPrev[coin][period][1]);
            console.log("gArrayPrev:" + coin +":" +period);
            array = setSort(array, optionsSort.sort, optionsSort.percent);
            io.emit('filters', JSON.stringify(array));
          }
        }
      }
    });

    console.info('subsc:',subw);
  }
})();

io.on('connection', (socket) => {
  var con = true;
	let endpoint = false;
	/*binance.prevDay(false, (error, prevDay) => {
    console.log(prevDay);
    io.emit('json', prevDay);
	});*/
	console.log('nuevo usuario');
  socket.on('disconnect', function () {
    con = false;
    
    console.log('user disconnect');
  });

  socket.on('offTa', function() {
    console.log('endp: ', endpoint);
    con = false;
    if (endpoint) binance.websockets.terminate(endpoint);
  });

  socket.on('offSort', function() {
    ssrt = false;
    optionsSort.currentArray = [];
    optionsSort.pairsArray = [];
    console.log('sort disconnect');
  })

  socket.on('main', function() {
    // if (con) {
      (async () => {
        const trades = await Trade.find().or([{status: 'PENDING'}, {status: 'COMPLETED'}]).sort({ _id:-1 });
        socket.emit('updateTrades', JSON.stringify(trades));
        console.log('main', trades.length);
      })();
    // }
  })

  socket.on('json', function (data) {
    con = true;
    (async () => {
      const trade = await Trade.findOne({symbol:data, status:'PENDING'});
      let orders = []
      if (trade) {
        if ( typeof trade.buy.order.id !== 'undefined' ) {
          var order = trade.buy;
          await orders.push(trade.buy);
        }
        if ( typeof trade.sell.order.id !== 'undefined' || typeof trade.sell.order.oco0.id !== 'undefined') {
          var order = trade.sell;
          await orders.push(trade.sell);
        }

        console.log('order update emit:\n',orders);
        // symbol, orderId, orderListId, clientOrderId, price, origQty, executedQty, cummulativeQuoteQty, status, timeInForce, type, side, stopPrice, icebergQty, time, updateTime, isWorking, origQuoteOrderQty
        if (orders.length > 0) socket.emit('onOrder', orders);
      }

    })();
    endpoint = binance.websockets.prevDay(data, (error, response) => {
      if (con) {
        socket.emit(`json_${data}`, response);
        // console.log(response);
      }
    });
    console.log('json endp:',endpoint);
  });

  socket.on('sort', function (data) {
    (async () => {
      sio = socket;
      ssrt = true;
      // console.log(data);
      let currentArray = JSON.parse(data.currentArray);
      optionsSort = {coin:data.symbol,period:data.period,percent:data.percent,sort:data.sort,currentArray,pairsArray:[]};
      /*if (currentArray.length == 0 && data.period == '1d' && data.percent == '0') {
        // console.log(gArrayPrev[data.symbol]);
      }*/
      let arrayTick = setSort(gArrayPrev[data.symbol][data.period], data.sort, data.percent);
      console.log('arrayTick', arrayTick.length);
      socket.emit('filters', JSON.stringify(arrayTick));
      // let cFilters = gArrayPrev[data.symbol][data.period].map(x => x.symbol);
      var dbSymbols = await Dsymbol.find({active: true});
      var arraySymbols = [];
      for (let obj of dbSymbols) {
        let symbol = obj.pair;
        var {search} = getCoin(symbol, data.symbol);
        if ( search !== -1 && search !== 0 ) arraySymbols.push(symbol);
      }
      console.log('arraySymbols', arraySymbols.length);
      console.log(`gArrayPrev ${data.symbol} ${data.period}`, gArrayPrev[data.symbol][data.period].length);

      let best_pairs = [];
      let i = 0;
      let f = 0;
      /*for (let symbol of arraySymbols) {
        binance.candlesticks(symbol, data.period, (error, ticks, symbol) => {
          let last_tick = ticks[ticks.length - 1];
          if (last_tick !== undefined) {
            let [time, open, high, low, close, volume, closeTime, assetVolume, trades, buyBaseVolume, buyAssetVolume, ignored] = last_tick;
            let obTick = {symbol, time, open, high, low, close, volume, closeTime, assetVolume, trades, buyBaseVolume, buyAssetVolume, ignored}
            obTick.symbol = symbol;
            let percentChange = ((parseFloat(obTick.close) / parseFloat(obTick.open)) - 1) * 100;
            obTick.percentChange = percentChange;
            obTick.assetVolume = parseFloat(obTick.assetVolume);
            // console.log(`${symbol}: `,objTick);
            let resultIndex = (best_pairs.length > 0) ? best_pairs.findIndex( ({ symbol }) => symbol === obTick.symbol ) : -1;
            if (resultIndex == -1) {
              best_pairs.push(obTick);
              // console.log(`${coin} ${period} ${gArrayPrev[coin][period].length}`);
            }else {
              best_pairs[resultIndex] = obTick;
            }

            i++;
          } else f++; 
          let totcount = i+f;


          if (arraySymbols.length == totcount) {
            console.log('best_pairs 1: ',best_pairs.length);
            if (best_pairs.length > 0) {
              best_pairs = setSort(best_pairs, data.sort, data.percent);
              // socket.emit('filters', JSON.stringify(best_pairs));
            }
            optionsSort.pairsArray = (best_pairs.length == 0) ? arrayTick : best_pairs;
            console.log('best_pairs 2: ',best_pairs.length);
            console.log("count: ",i);
            console.log("current: ", JSON.parse(data.currentArray).length);
          }
        }, {limit: 2});
      }*/
      // let symbols = await Dsymbol.find({ "pair": /BNB/i });      
    })();
  });
});

/*(async () => {
  var fTrade = await Trade.findOne({'status': 'PENDING'});
  if (fTrade) await fTrade.save();

})();*/
server.listen(3000);


function balance_update(data) {
  console.log("Balance Update");
  console.log(data.a.B);
  console.log(data.a.P);
  return;
  // console.log(data);
  // console.log('coins: ',data.B.length);
  (async () => {
    for ( let obj of data.B ) {
      let { a:asset, f:available, l:onOrder } = obj;
      let bl = await Balance.findOne({asset});
      if (bl) {
        if (bl.available != available || bl.onOrder != onOrder) {
          bl.available = available;
          bl.onOrder = onOrder;
          await bl.save();
        }
      } else {
        const newBalance = new Balance({ asset, available, onOrder });
        await newBalance.save();
        // console.log(newBalance);
      }
      if ( available != "0.00000000" || onOrder != "0.00000000" ) /*continue;*/
        console.log(asset+"\tavailable: "+available+" ("+onOrder+" on order)");
    }
  })();
}
function execution_update(data) {
  (async () => {
    console.info( await binance.futuresUserTrades( "BTCUSDT" ) );
  })();
  console.log("Execution Update");
  console.log(data);
  return;
  let { x:executionType, s:symbol, p:price, P:stopPrice, q:quantity, S:side, o:orderType, i:orderId, X:orderStatus, g: orderListId } = data;
  if ( executionType == "NEW" ) {
    if ( orderStatus == "REJECTED" ) {
      console.log("Order Failed! Reason: "+data.r);
    }
    console.log(symbol+" "+side+" "+orderType+" ORDER #"+orderId+" ("+orderStatus+")");
    console.log("..price: "+price+", quantity: "+quantity);
  }
  let status = 'PENDING';
  let stOrder = {'buy.order.id':orderId};
  let result_trade = { symbol, price:{}, buy:{}, sell:{} };
  if (side == 'BUY') {
    result_trade.buy = {active: 'true', limit: price, amount: quantity};
    result_trade.buy.order = { id:orderId, status:orderStatus };
    result_trade.price.entry = price;
    result_trade.sell = {type:'0', active: 'false'};
    if (orderStatus == 'CANCELED') result_trade.status = 'CANCELED';
  }
  if (side == 'SELL') {
    if (orderStatus == 'FILLED') status = 'COMPLETED';
    // else if (orderStatus == 'CANCELED')
    let type = '2';
    let psc = {limit: price};
    switch (orderType) {
      case 'LIMIT':
        type = '0';
        break;
      case 'STOP_LOSS_LIMIT':
        if (orderListId == -1) type = '1';
        psc = {stop:stopPrice,stop_limit:price};
        break;
    }
    result_trade.buy.active = 'false';
    result_trade.price.exit = price;
    result_trade.sell = psc;
    result_trade.sell.type = type;
    result_trade.sell.active = 'true';

    let order = { id:orderId, status:orderStatus };
    stOrder = {'sell.order.id':orderId};
    if (type == '2') {
      result_trade.sell.order = {id:orderListId};
      if (orderType == 'STOP_LOSS_LIMIT') {
        stOrder = {'sell.order.oco0.id':orderId};
        result_trade.sell.order.oco0 = order;
        result_trade.sell.stop = stopPrice;
        result_trade.sell.stop_limit = price;
      } else {
        stOrder = {'sell.order.oco1.id':orderId};
        result_trade.sell.order.oco1 = order;
        result_trade.sell.limit = price;
      }
    } else result_trade.sell.order = order;
    result_trade.sell.amount = quantity;
  }
  result_trade.status = status;
  console.log(result_trade);
  // var ob = Object.assign({},doc.price,result_trade.price);
  (async () => {
  // console.log(data);
    let re = {};
    let extr = await Trade.findOne({symbol, status: 'PENDING'});
    let trByOrderId = await Trade.findOne(stOrder);
    // orderStatus !== 'CANCELED' && 
    if (!trByOrderId || (trByOrderId && trByOrderId.status == 'PENDING')) {
      if (extr) {
        let dtr = await Trade.findOne({_id: extr._id}).select('-_id -date -__v');
        re = mergeTrade(dtr, result_trade);
        if (re.sell.type == '2' && typeof re.sell.order.status !== 'undefined') delete re.sell.order.status;
        else if (typeof re.sell.order !== 'undefined' && re.sell.type !== '2') {
          if (typeof re.sell.order.oco0 !== 'undefined') delete re.sell.order.oco0
          if (typeof re.sell.order.oco1 !== 'undefined') delete re.sell.order.oco1
        }
        if (extr.sell.active == 'true')  re.sell.active = 'true';
        if (extr.sell.type !== re.sell.type && side == 'BUY') re.sell.type = extr.sell.type;
        extr.overwrite(re);
        await extr.save();
        // console.log("res:", re);
      } else {
        let nwTrade = new Trade(result_trade);
        extr = await nwTrade.save();
        console.log(nwTrade);
      }
    }
    await emitUpdateTrades();
    io.emit(`${symbol}_status`, {status: result_trade.status});
    if (orderStatus == 'FILLED') {
      io.emit(`${symbol}_filled`, {side});
      if (side == "BUY" && re.sell.active == 'true') {
        let typeOrder = {type: 'LIMIT'};
        var priceLimit = re.sell.limit;
        switch (extr.sell.type) {
          case '1':
              priceLimit = re.sell.stop_limit;
              typeOrder = {stopPrice: re.sell.stop, type: 'STOP_LOSS_LIMIT'};
              break;
          case '2':
              typeOrder = {type: 'OCO', stopLimitPrice: re.sell.stop_limit, stopPrice: re.sell.stop};
              break;
        }
        try {
          var {coin, search} = getCoin(re.symbol);
          if (search !== -1 && search !== 0) {
            var asset = (re.symbol).split(coin)[0];
            let bl = await Balance.findOne({asset});
            if (parseFloat(quantity) != parseFloat(bl.available) && parseFloat(bl.available) > 0) {
              let ds = Dsymbol.findOne({pair:re.symbol});
              re.sell.amount = formatNumber(quantity, ds.dec_qty);
            }
          }
          let tSell = await binance.sell(re.symbol, re.sell.amount, priceLimit, typeOrder);
          console.log('order status: ', tSell);
        } catch(e) {
          var error = e;
          if (e.body) error = e.body;
          console.log('812 error: ',error);
        }
        // if (extr.sell.type == '2') {
        //   extr.sell.order = { 
        //     id: tSell.orderId;
        //     oco0: {id: tSell.orderReports[0].orderId, status: tSell.orderReports[0].status},
        //     oco1: {id: tSell.orderReports[1].orderId, status: tSell.orderReports[1].status}
        //   };
        // }
        // tSell = await binance.orderStatus(re.symbol, tSell.orderId);
        // extr.sell.order.id = tSell.orderId;
        // extr.sell.order.status = tSell.status;
      }
    }
  })();
  //NEW, CANCELED, REPLACED, REJECTED, TRADE, EXPIRED
  // console.log(symbol+"\t"+side+" "+executionType+" "+orderType+" ORDER #"+orderId);
}

// binance.websockets.userData(balance_update, execution_update);

function f_balance_update(data) {
  console.log("Balance Future Update");
  console.log(data.a.B);
  // console.log(data.a.P);
}

function f_execution_update(data) {
  let { x:executionType, s:symbol, p:price, sp:stopPrice, q:quantity, 
    S:side, o:orderType, i:orderId, X:orderStatus, t: tradeId, ps: positionSide,
    z: acQty, ot: orgOrderType } = data.o;
  // let positionSide;
  (async () => {
    let fsymbol = symbol;
    let forder = await binance.futuresOrderStatus( symbol, {orderId} );
    let ftrade = await binance.futuresUserTrades( symbol , {fromId: tradeId});
    let fbalance = await fbinance.futuresBalance();
    let pRiskIndex = (await binance.futuresPositionRisk()).findIndex( ({ symbol }) => symbol === fsymbol );
    let positionRisk = (await binance.futuresPositionRisk())[pRiskIndex];
    let leverage = positionRisk.leverage;
    let openOrders = await binance.futuresOpenOrders(symbol);
    // console.log('posrisk', positionRisk);
    console.log('future execution');
    console.log('data', data);
    console.log('forder', forder);
    // console.log('fbalance', fbalance);
    
    // let positionSide forder.positionSide == 'SHORT' ? 
    // console.info(  );
    if ((orderStatus == 'CANCELED' || forder.status == 'EXPIRED') && ((side == 'BUY' && positionSide == 'LONG') || (side == 'SELL' && positionSide == 'SHORT'))) {
      // side.toLowerCase() + '.order.id'
      let delOrder = side == 'BUY' ? {'buy.order.id': orderId} : {'sell.order.id': orderId};
      let oc = await Trade.deleteOne(delOrder);
      console.log('order cancel:', oc);
      return;
    }
    let query = {symbol, status: 'PENDING', market: 'futures', positionSide: positionSide.toLowerCase()};
    let byOrderId;
    if ( side == 'BUY' ) {
      byOrderId = {'buy.order.id': orderId};
      /*if (orderStatus == 'NEW' && positionSide == 'SHORT')*/ query.quantity = quantity;
      // if (positionSide == 'LONG' || (orderStatus !== 'NEW' && positionSide == 'SHORT')) query['buy.order.id'] = orderId;
    } else if ( side == 'SELL' ) {
      byOrderId = {'sell.order.id': orderId};
      /*if (orderStatus == 'NEW' && positionSide == 'LONG')*/ query.quantity = quantity;
      // if (positionSide == 'SHORT' || (orderStatus !== 'NEW' && positionSide == 'LONG')) query['sell.order.id'] = orderId;
    }
    let ftr = await Trade.findOne(query);
    let findByOrderId = await Trade.find(byOrderId);
    query = {};
    // save orders close
    if (ftr && ((positionSide == 'LONG' && side == 'SELL') || (positionSide == 'SHORT' && side == 'BUY'))) {
      if (orgOrderType == 'STOP') ftr[side.toLowerCase()].order.oco1 = { id: orderId, status: orderStatus };
      else ftr[side.toLowerCase()].order.oco0 = { id: orderId, status: orderStatus };
      await ftr.save();
      console.log('EXISTE, registrado: ', orgOrderType);
    }

    if (findByOrderId > 0) console.log('EXISTE BY ID');
    var orderExit = false;

    if (ftr && tradeId !== 0 && typeof ftrade[0].price != 'undefined') {
      // la orden se ha ejecutado
      // update
      if (side == 'BUY') {        
        ftr.buy.amount = acQty;

        if (positionSide == 'SHORT') {
          if (parseFloat(ftr.price.entry) > parseFloat(ftrade[0].price)) ftr.buy.limit = ftrade[0].price;
          else ftr.buy.stop_limit = ftrade[0].price;
          var sc = 0;
          openOrders.forEach( sr => {
            // var vrfPrice = orderType == 'STOP' ? ftr.buy.stop_limit : ftr.buy.limit;
            if (sr.price == ftrade[0].price) sc++;
          });
          if ((orderStatus == 'FILLED' || orderStatus == 'PARTIALLY_FILLED') && (sc == 0 || acQty == quantity)) ftr.status = "COMPLETED";
          /*else if (orderStatus == 'NEW')*/ 
          ftr.buy.order.id = orderId;
          ftr.buy.order.status = orderStatus;
        } else {
          // ejecutar la orden profit/stoploss, es LONG Y SE LLENO
          ftr.buy.limit = ftrade[0].price;
          var lcount = 0;
          var vrfLongExit = typeof ftr.sell.stop !== 'undefined' && parseFloat(ftr.sell.stop) != 0 && parseFloat(ftr.sell.stop) != parseFloat(ftr.buy.limit);
          openOrders.forEach( lorder => {
            if (lorder.price == ftr.buy.limit) lcount++;
          });
          if ((orderStatus == 'FILLED' || orderStatus == 'PARTIALLY_FILLED') && lcount == 0 && vrfLongExit) {
            orderExit = true;
            ftr.quantity = acQty;
            sendNotification(`Open Operation ${symbol}`, `${symbol} Entry ${price} - ${positionSide}`);
          }
        }
        ftr.buy.order.status = orderStatus;
      }

      if (side == 'SELL') {
        ftr.sell.amount = acQty;

        if (positionSide == 'LONG') {
          if (parseFloat(ftr.price.entry) < parseFloat(ftrade[0].price)) ftr.sell.limit = ftrade[0].price;
          else ftr.sell.stop_limit = ftrade[0].price;
          var lc = 0;
          openOrders.forEach( lr => {
            // var vrfPrice = orderType == 'STOP' ? ftr.buy.stop_limit : ftr.buy.limit;
            if (lr.price == ftrade[0].price) lc++;
          });
          if ((orderStatus == 'FILLED' || orderStatus == 'PARTIALLY_FILLED') && (lc == 0 || acQty == quantity)) ftr.status = "COMPLETED";
          /*else if (orderStatus == 'NEW')*/
          ftr.sell.order.id = orderId;
          ftr.sell.status = orderStatus;
        } else {
          // ejecutar la orden profit/stoploss, es SHORT Y SE LLENO
          ftr.sell.limit = ftrade[0].price;
          var scount = 0;
          openOrders.forEach( sorder => {
            if (sorder.price == ftrade[0].price) scount++;
          });
          if ((orderStatus == 'FILLED' || orderStatus == 'PARTIALLY_FILLED') && scount == 0 && typeof ftr.buy.stop != 'undefined' &&
            parseFloat(ftr.buy.stop) != 0 && parseFloat(ftr.buy.stop) != parseFloat(ftr.sell.limit)) {
            orderExit = true;
            ftr.quantity = acQty;
            sendNotification(`Open Operation ${symbol}`, `${symbol} Entry ${price} - ${positionSide}`);
          }
        }
        ftr.sell.order.status = orderStatus;
      }

      if (ftr.status == "COMPLETED") {
        ftr.price.exit = ftrade[0].price;
        ftr.balance.final = fbalance[0].balance;
        console.log('Completado');
        sendNotification(`Close Operation ${symbol} - ${positionSide}`, `${symbol} Entry ${ftr.price.entry} | Exit ${ftr.price.exit}`);
      }
      await ftr.save();
      console.info('update', ftr);
    } else if (!ftr && findByOrderId == 0 && tradeId !== 0) {
      // new

      let queryTr = {
        symbol,
        status: 'PENDING',
        positionSide: positionSide.toLowerCase(),
        market: 'futures',
        quantity,
        leverage,
        balance: { initial: fbalance[0].balance },
        price: {entry: ftrade[0].price },
        buy: {
          type: '0',
          active: 'true',
          limit: side == 'BUY' ? ftrade[0].price : '0',
          amount: '0'
        },
        sell: {
          type: '0',
          active: 'true',
          limit: side == 'SELL' ? ftrade[0].price : '0',
          amount: '0'
        },
      };
      var qorder = { id: orderId, status: orderStatus };
      if (side == 'BUY') queryTr.buy.order = qorder;
      if (side == 'SELL') queryTr.sell.order = qorder;

      let newFtr = new Trade(queryTr);
      let nftr = await newFtr.save();
      console.info('new', nftr);
    }

    if (ftr) {
      var exs, exp = false;
      var oside = ftr.positionSide == 'long' ? 'SELL' : 'BUY';
      var vrftr = ftr[oside.toLowerCase()];
      
      // Reiniciar orden al expirar
      if (ftr.status != 'COMPLETED' && orderStatus == 'EXPIRED' && 
        ((positionSide == 'LONG' && side == 'SELL') || (positionSide == 'SHORT' && side == 'BUY'))) {
        console.info('Expired order: ');
        let daily = await binance.futuresDaily(symbol);
        let conditionOrder = {};
        var expPrice = price;
        if (orgOrderType == 'STOP') { //restart stoploss
          console.info('-> restart StopLoss');
          if ((daily.lastPrice > stopPrice && positionSide == 'LONG') || 
          (daily.lastPrice < stopPrice && positionSide == 'SHORT')) {//mayor: stop limit
            expPrice = stopPrice;
            conditionOrder = {
                positionSide: positionSide, 
                type: 'STOP', 
                stopPrice: stopPrice
            };
          } else if ((daily.lastPrice <= stopPrice && positionSide == 'LONG') ||
          (daily.lastPrice >= stopPrice && positionSide == 'SHORT'))  {//menor igual: market
            expPrice = false;
            conditionOrder = {positionSide: positionSide};
          }
        } else {//restart profit
          console.info('-> restart Profit');
          if (daily.lastPrice > price) expPrice = daily.lastPrice;
          conditionOrder = {positionSide: positionSide};
        }
        try {
          await binance.futuresOrder(side, symbol, quantity, expPrice, conditionOrder);
        } catch (e) {
          console.error('error order Expired', e);
        }
      }

      //verificar ordenes oco stoploss
      if (typeof vrftr.order.oco1 !== 'undefined') {
        openOrders.forEach( ord => {
          // var vrfPrice = orderType == 'STOP' ? ftr.buy.stop_limit : ftr.buy.limit;
          if (ord.origType == 'STOP' && ord.orderId == vrftr.order.oco1.id && ord.price == vrftr.stop_limit) exs = true;
          if (ord.origType != 'STOP' && ord.orderId == vrftr.order.oco0.id && ord.price == vrftr.limit) exp = true;
        });
      }

      // Ejecutar ordenes de salida pnl
      if ((orderExit ) && ftr.status != 'COMPLETED') {
        try {
          console.log('Stoploss executed');
          let closeStopOrder = await binance.futuresOrder(
            oside, symbol, ftr.quantity, ftr[oside.toLowerCase()].stop_limit,
            {
              positionSide: ftr.positionSide, 
              type: 'STOP', 
              stopPrice: ftr[oside.toLowerCase()].stop
          });
        } catch (e) {
          console.log('stoploss error: ', e);
        }
      } else if (exs && !exp && ftr.status != 'COMPLETED') {
        try {
          console.log('Profit executed');
          let closePriceOrder = await binance.futuresOrder(
            oside, symbol, ftr.quantity, ftr[oside.toLowerCase()].limit,
            {positionSide: ftr.positionSide});
        } catch (e) {
          console.log('profit error: ', e);
        }
      }


    }

  })();
}
fbinance.websockets.userFutureData(f_balance_update, f_execution_update);

function digitPrice (price) {
  var str = Array.from(price);
  for (var i = str.length - 1; i >= 0; i--) {
    if (str[i] == '0') {
      str.pop();
    } else {
      break;
    }
  }
  var result = ((str.join("")).split('.')[1]).length;
  return result;
}

function formatNumber (num, dig) {
  var g = parseInt(dig);
  var format = parseFloat(num).toFixed(g);

  while (format > parseFloat(num)) {
    var d = format.split(".");
    if (typeof d[1] != 'undefined') {
      //num = parseInt(num.split(".")[1]).toFixed(g);
        d[1] = d[1]-1;
        format = parseFloat(d.join("."));
    } else {
      format = format-1;
    }
  }

  return format.toString();
}

function getCoin(symbol, coin = false) {
  // solusdc
  // bnbusdc usdt
  var arrCoins = ['BTC','ETH','USDT','BNB'];
  var i = 0;
  var search = -1;
  if (!coin) {
    while ((search === -1 || search === 0) && i < arrCoins.length) {
      coin = arrCoins[i];
      search = symbol.search(coin);
      i++;
    }
  } else search = symbol.search(coin);
  return {coin,search};
}

function setSort(array, option, sP) {
  var col = {pair: "symbol", chg: "percentChange", vol: "assetVolume", tr: "trades"};
  var best_pairs = [];
  if (sP !== '0') {
    for (let obj of array) {
      var cond = true;
      switch (sP) {
        case '1':
          cond =  obj.percentChange > 0;
          break;
        case '2':
          cond =  obj.percentChange < 0;
          break;
      }
      if (cond) best_pairs.push(obj);
    }
  } else best_pairs = array;
  best_pairs.sort((a, b) => (a[col['vol']] < b[col['vol']]) ? 1 : -1);
  if (option != 'false') {
    var sort = (option).split('_');
    switch (sort[1]) {
      case 'asc':
        best_pairs.sort((a, b) => (a[col[sort[0]]] > b[col[sort[0]]]) ? 1 : -1);
        break;
      case 'desc':
        best_pairs.sort((a, b) => (a[col[sort[0]]] < b[col[sort[0]]]) ? 1 : -1);
        break;
    }
  }

  return best_pairs;
}

function simpleTick (candlesticks) {
  let { e:eventType, E:eventTime, s:symbol, k:ticks } = candlesticks;
  let { o:open, h:high, l:low, c:close, v:volume, n:trades, i:interval, x:isFinal, q:assetVolume, V:buyVolume, Q:quoteBuyVolume } = ticks;
  var objTick = {symbol,open,high,low,close,volume,trades,interval,isFinal,assetVolume,buyVolume,quoteBuyVolume};
  var percentChange = ((parseFloat(objTick.close) / parseFloat(objTick.open)) - 1) * 100;
  objTick.assetVolume = parseFloat(objTick.assetVolume);
  objTick.percentChange = percentChange;

  return objTick;
}

async function updateSymbols (tick) {
  let qty = 0;
  let price = 0;
  var bidQty = typeof tick.bidQty !== 'undefined' ? tick.bidQty : tick.bestBidQty

  var d0 = digitPrice(`${typeof tick.bidQty !== 'undefined' ? tick.bidQty : tick.bestBidQty}`);
  var d1 = digitPrice(`${typeof tick.askQty !== 'undefined' ? tick.askQty : tick.bestAskQty}`);
  var p0 = digitPrice(`${typeof tick.bidPrice !== 'undefined' ? tick.bidPrice : tick.bestBid}`);
  var p1 = digitPrice(`${typeof tick.askPrice !== 'undefined' ? tick.askPrice : tick.bestAsk}`);
  if (d0 == d1) qty = d0;
  else if(d0 > d1) qty = d0;
  else qty = d1;

  if (p0 == p1) price = p0;
  else if(p0 > p1) price = p0;
  else price = p1;

  try {
    let decc = await Dsymbol.findOne({pair: tick.symbol});
    let condActive = (parseFloat(typeof tick.priceChangePercent !== 'undefined' ? tick.priceChangePercent : tick.percentChange) == 0 && 
                          parseFloat(tick.quoteVolume) == 0) ||
                      parseFloat(typeof tick.lastPrice !== 'undefined' ? tick.lastPrice : tick.close) == 0;
                      
    // console.log(condActive);
    if (decc && (parseInt(decc.digits) < parseInt(price) || parseInt(decc.dec_qty) < parseInt(qty) || decc.active != !condActive)) {
      if (condActive && decc.pair == tick.symbol) decc.active = false;
      decc.digits = price;
      decc.dec_qty = qty;
      await decc.save();
      // console.log(decc);
    } else if (!decc && !condActive) {
      decc = new Dsymbol({
        pair: tick.symbol,
        digits: price,
        dec_qty: qty
      });
      await decc.save();
    }
  } catch (e) {
    console.log(tick.symbol);
    var error = e;
    if (e.body) error = e.body
    console.log('error updateSymbols: ', error);
  }
  
}

function mergeTrade(arr1, arr2) {
	// var res = {symbol:'',status:'',price:{},buy:{active:'',limit:'',amount:'',order:{}},sell:{type:'',active:'',limit:'',stop:'',stop_limit:'',amount:'',order:{id:'',status:'',oco0:{},oco1:{}}}};
  var res = {};
  var ems = "nothing";
  var arg1 = arr1.toJSON();
  var arg2 = arr2;
  console.log("arr1", arr1);
  console.log("arr2", arr2);
  try {
    console.log(Object.keys(arg1));
  	Object.keys(arg1).forEach(k => {
        ems = `${k}`;
        // if (k == '$__') continue;
        // res[k] = {};
      	if (typeof res[k] == 'undefined') {
          res = Object.assign({},arg1,arr2);
        }
          
        console.log("key:", k);
      	if (typeof arg1[k] == 'object' && arg1[k] != null && typeof arg1[k] !== 'undefined') {
        	Object.keys(arg1[k]).forEach( i => {
            console.log("\tsubkey:", i);
            ems = `${k} ${i}`;
            // if (i == 'selected') continue;
         	  if (typeof res[k][i] == 'undefined' && typeof arr2[k] != 'undefined') {
              res[k] = Object.assign({},arg1[k],arr2[k]);
           	  // res[k][i] = {};
            }
           	if (typeof arg1[k][i] === 'object' && arg1[k][i] !== null && typeof arg1[k][i] !== 'undefined') {
              if (typeof arr2[k][i] !== 'undefined') res[k][i] = Object.assign({},arg1[k][i],arr2[k][i]);
       	      //else res[k][i] = typeof arr2[k][i] === 'undefined' ? arg1[k][i] : arr2[k][i];
           	}
        	});
      	}
    	});
  } catch (e) {
    console.log("error merge", e);
    console.log("ems:", ems);
  }
  console.log("res",res);
  return res;
}

async function emitUpdateTrades () {
  const trades = await Trade.find().or([{status: 'PENDING'}, {status: 'COMPLETED'}]);
  io.emit('updateTrades', JSON.stringify(trades));
}

function sendNotification(title, body) {
  // `Open Operation ${symbol}`
  // `${symbol} Entry ${price} - ${positionSide}`
  let message = {
    data: {title,body}
  };
  
  let options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
  }

  fbsadmin.messaging().sendToTopic("crypto_pro", message, options)
  .then((response) => {
    console.log('Successfully sent message:', response);
  })
  .catch((error) => {
    console.log('Error sending message:', error)
  });
}