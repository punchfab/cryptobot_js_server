require('dotenv').config();
const jwt = require('jsonwebtoken');
const User = require('../models/user');

async function verifyToken(req, res, next) {
    const token = req.headers['x-access-token'];
    if (!token) {
    	console.log("token false:", token);
        return res.status(401).send({ auth: false });
    }
    // Decode the Tokenreq.userId = decoded.id;
    const decoded = await jwt.verify(token, process.env.SECRET);
    // console.log(decoded);
    const user = await User.findById(decoded.id, { password: 0});
    //var access = true;
  	if (!user) {
  		console.log("user not found:", token);
  		return res.status(404).send("Not found.");
  	}
    next();
}

module.exports = verifyToken;