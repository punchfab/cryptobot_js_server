/**
 * Used as part of the user data websockets callback
 * @param {object} data - user data callback data type
 * @return {undefined}
 */
const userFutureDataHandler = data => {
    let type = data.e;
    if ( type === 'ACCOUNT_UPDATE' ) {
        Binance.options.future_balance_callback( data );
    } else if ( type === 'ORDER_TRADE_UPDATE' ) {
        if ( Binance.options.future_execution_callback ) Binance.options.future_execution_callback( data );
    } else if ( type === 'listenKeyExpired' ) {
        // Binance.websockets.userFutureData( Binance.options.future_balance_callback, Binance.options.future_execution_callback );
        Binance.options.future_expired = true;
    } else {
        Binance.options.log( 'Unexpected userFutureData: ' + type );
    }
};


/**
* Future Userdata websockets function
* @param {function} callback - the callback function
* @param {function} execution_callback - optional execution callback
* @param {function} subscribed_callback - subscription callback
* @param {function} list_status_callback - status callback
* @return {undefined}
*/
userFutureData: function userFutureData( callback, execution_callback = false, subscribed_callback = false/*, list_status_callback = false*/ ) {
    Binance.options.future_expired = false;
    let reconnect = () => {
        if ( Binance.options.reconnect ) userFutureData( callback, execution_callback, subscribed_callback );
    };
    apiRequest( fapi + 'v1/listenKey', {}, function ( error, response ) {
        console.log('listenKey', response);
        console.log('err listenKey', error);
        Binance.options.listenFutureKey = response.listenKey;
        Binance.options.timestampFuture = Date.now();
        setTimeout( function userDataKeepAlive() { // keepalive
            try {
                if (Binance.options.future_expired) userFutureData( callback, execution_callback, subscribed_callback );
                console.log('userDataKeepAlive...');
                apiRequest( fapi + 'v1/listenKey', {}, function ( err ) {
                    if ( err ) setTimeout( userDataKeepAlive, 60000 ); // retry in 1 minute
                    else setTimeout( userDataKeepAlive, 60 * 60 * 1000 ); // 60 minute keepalive
                }, 'PUT' );
            } catch ( error ) {
                setTimeout( userDataKeepAlive, 60000 ); // retry in 1 minute
            }
        }, 60 * 60 * 1000 ); // 60 minute keepalive
        Binance.options.future_balance_callback = callback;
        Binance.options.future_execution_callback = execution_callback;
        // Binance.options.future_list_status_callback = list_status_callback;
        const subscription = subscribe( Binance.options.listenFutureKey, userFutureDataHandler, reconnect );
        if ( subscribed_callback ) subscribed_callback( subscription.endpoint );
    }, 'POST' );
},