const mongoose = require('mongoose');
var url = "mongodb://localhost:27017/cryptobot";

mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.connect(url, {
	useUnifiedTopology: true,
	useNewUrlParser: true
	// useFindAndModify: false
})
.then(db => console.log("DB is connected!"))
.catch(err => console.error(err));
// database