const mongoose = require('mongoose');
const { Schema } = mongoose;

const SymbolSchema = new Schema ({
	pair: { type: String, required: true, unique: true },
	digits: { type: Number },
	dec_qty: { type: Number},
	active: { type: Boolean, default: true},
	date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Symbol', SymbolSchema);