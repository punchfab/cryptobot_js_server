const mongoose = require('mongoose');
const { Schema } = mongoose;

const BalanceSchema = new Schema ({
	asset: { type: String, required: true, unique: true },
	available: { type: String },
	onOrder: { type: String },
	active: { type: Boolean, default: true },
	date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Balance', BalanceSchema);