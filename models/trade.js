const mongoose = require('mongoose');
const { Schema } = mongoose;
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
// const Binance = require('node-binance-api');
/*const binance = new Binance().options({
  APIKEY: 'aaN6u4MU5SM61O0cCMTSV82GYvRL8iyZyoHGyLLuqrxKFE8I0Z8AecfykuF0QsBd',
  APISECRET: 'gYtkcwiePMxFRZ9tNw0PjQTd5ZfjMPoVfc0uuiwmAx0KahGS5DVcsOt3Aj17PcBP'
});*/
const sleep = (seconds) => new Promise(resolve => setTimeout(resolve, seconds * 1000));

mongoose.connect('mongodb://localhost:27017/cryptobot', { useFindAndModify: false, useUnifiedTopology: true,useNewUrlParser: true});

const TradeSchema = new mongoose.Schema ({
	symbol: { type: String, required: true },
	status: { type: String },
	positionSide: { type: String, default: 'long' },
	market: { type: String, default: 'spot' },
	quantity: { type: String },
	leverage: { type: String },
	balance: {
		initial: { type: String },
		final: { type: String }
	},
	price: {
		entry: { type: String },
		exit: { type: String }
	},
	buy: {
		type: { type: String },
		active: { type: String },
		limit: { type: String },
		stop: { type: String },
		stop_limit: { type: String },
		amount: { type: String },
		order: {
			id: { type: String },
			status: { type: String },
			oco0: {
				id: { type: String},
				status: { type: String }
			},
			oco1: {
				id: { type: String},
				status: { type: String }	
			}
		}
	},
	sell: {
		type: { type: String },
		active: { type: String },
		limit: { type: String },
		stop: { type: String },
		stop_limit: { type: String },
		amount: { type: String },
		order: {
			id: { type: String },
			status: { type: String },
			oco0: {
				id: { type: String},
				status: { type: String }
			},
			oco1: {
				id: { type: String},
				status: { type: String }	
			}
		}	
	},
	date: { type: Date, default: Date.now }
});

/*TradeSchema.post('save', function(doc){
	(async () => {
		const dbTrade = mongoose.model('Trade', TradeSchema);
		// const mTrade = await dbTrade.findOne({_id: doc._id});
		
		const trades = await dbTrade.find().or([{status: 'PENDING'}, {status: 'COMPLETED'}]);
        io.emit('updateTrades', JSON.stringify(trades));

		if (mTrade.status == 'PENDING') {
			try {
				await onTrade(doc);
			} catch (e) {
				var error = e;
				if (e.body) error = e.body;
				console.log('69 error: ', error);
				// const mTrade = await dbTrade.findOne({_id: doc._id});
				mTrade.doc = doc.symbol;
				// await mTrade.save();
			}
		}

  		console.log('trade recibido: ',doc);
	})();
});*/

// server.listen(3030);

async function onTrade (doc) {
	const dbTrade = mongoose.model('Trade', TradeSchema);
	const mTrade = await dbTrade.findOne({_id: doc._id});
	let orderId;
	let orderId2 = null;
	let typeOrder = {type: 'LIMIT'};
	var status;
	var side;
	var priceLimit = mTrade.sell.limit;

	switch (mTrade.sell.type) {
		case '1':
			priceLimit = mTrade.sell.stop_limit;
	  		typeOrder = {stopPrice: mTrade.sell.stop, type: 'STOP_LOSS_LIMIT'};
	  		break;
		case '2':
	  		typeOrder = {type: 'OCO', stopLimitPrice: mTrade.sell.stop_limit, stopPrice: mTrade.sell.stop};
	  		break;
	}
	// compra nueva
	if (typeof doc.buy.order.id !== 'undefined' && doc.buy.order.status != 'FILLED') orderId = doc.buy.order.id;
	// venta nueva
	if (typeof doc.sell.order.id !== 'undefined' && doc.sell.order.status != 'FILLED') orderId = doc.sell.order.id;
  	// console.log(order.symbol+" order status:", order.status);
  	if (typeof doc.sell.order.oco0.id !== 'undefined') {
  		let order0 = await binance.orderStatus(doc.symbol, doc.sell.order.oco0.id);
  		let order1 = await binance.orderStatus(doc.symbol, doc.sell.order.oco1.id);

  		do {
  			// trades OCO
  			order0 = await binance.orderStatus(doc.symbol, doc.sell.order.oco0.id);
  			order1 = await binance.orderStatus(doc.symbol, doc.sell.order.oco1.id);

  			if (order0.status == 'FILLED') {
  				mTrade.price.exit = mTrade.sell.stop_limit;
  				mTrade.sell.order.oco0.status = order0.status;
  			}
  			if (order1.status == 'FILLED') {
  				mTrade.price.exit = mTrade.sell.limit;
  				mTrade.sell.order.oco1.status = order1.status;
  			}

  			if (order0.status == 'FILLED' || order1.status == 'FILLED') {
  				mTrade.status = 'COMPLETED';
  				await mTrade.save();
  				io.emit(`${doc.symbol}_status`, {status:'COMPLETED'});
  				io.emit(`${doc.symbol}_filled`, {side:'sell'});
  			}
  			console.log('ejecutando trade OCO..');
  			await sleep(10);
  		} while (order0.status == 'NEW' && order1.status == 'NEW' && mTrade.status !== 'CANCELED');
  	} else {
	  	let order = await binance.orderStatus(doc.symbol, orderId);
	  	// if (order.status !== 'CANCELED') {
	  	do {
	  		order = await binance.orderStatus(doc.symbol, orderId);
	  		if (order.status == 'FILLED') {
	  			// se ha llenado
	  			if (order.side == 'BUY') {
	  				// es compra
	  				console.log("BUY FILLED");
	  				mTrade.buy.order.status = order.status;
	  				if (mTrade.sell.active == 'true') {
	  					// la venta esta activa procede orden
	  					try {
							let tSell = await binance.sell(mTrade.symbol, mTrade.sell.amount, priceLimit, typeOrder);
							console.log('sell: ',tSell);
							if (mTrade.sell.type == '2') {
								// ejecutar venta oco
								mTrade.sell.order = {
									oco0: {
										id: tSell.orderReports[0].orderId,
										status: tSell.orderReports[0].status
									},
									oco1: {
										id: tSell.orderReports[1].orderId,
										status: tSell.orderReports[1].status
									}
								}
							} else {
								// ejecutar venta limit, stop loss
								tSell = await binance.orderStatus(mTrade.symbol, tSell.orderId);
				            	mTrade.sell.order.id = tSell.orderId;
				            	mTrade.sell.order.status = tSell.status;
				            }
	  					} catch (e) {
	  						var error = e;
	  						if (e.body) error = e.body;
	  						console.log(priceLimit, typeOrder);
	  						console.log('165: ',error);
	  					}
	  				}
	  				io.emit(`${doc.symbol}_filled`, {side:'buy'});
	  				io.emit(`${doc.symbol}_status`, {status:'PENDING'});
	  			} else {
	  				console.log("SELL FILLED");
	  				// es venta
	  				mTrade.sell.order.status = order.status;
	  				mTrade.status = 'COMPLETED';
	  				io.emit(`${doc.symbol}_status`, {status:'COMPLETED'});
	  				io.emit(`${doc.symbol}_filled`, {side:'sell'});
	  			}
	  			await mTrade.save();
	  		} else if (order.status != 'NEW' && order.status != 'CANCELED') {
	  			// no se lleno enviar una notificacion
	  			console.log('Transaccion no se lleno');
	  		}
	  		console.log('ejecutando trade 1..');
	  		await sleep(10);
	  	} while (order.status == 'NEW');

	  	/*
	  	La operacion de compra y venta estan activa y la compra ya se lleno o
	  	La solo la operacion de venta esta activa y no se ha llenado
	  	*/
		if ((order.side == 'BUY' && mTrade.sell.active == 'true' && mTrade.buy.order.status == 'FILLED') || (mTrade.sell.active == 'true' && mTrade.buy.active == 'false')) {
			order = await binance.orderStatus(mTrade.symbol, mTrade.sell.order.id);
			do {
				order = await binance.orderStatus(mTrade.symbol, mTrade.sell.order.id);
				if (order.status !== 'CANCELED') {
					if (order.status == 'FILLED') {
						// la venta se completo al 100%
						mTrade.sell.order.status = order.status;
						mTrade.status = 'COMPLETED';
						// enviar emit al cliente
						io.emit(`${doc.symbol}_filled`, {side:'sell'});
						io.emit(`${doc.symbol}_status`, {status:'COMPLETED'});
					} else {
						// no se lleno alertar
						console.log('Transaccion no se lleno');
					}
				}
				console.log('ejecutando venta 2..');
				await sleep(10);
			} while (order.status == 'NEW');
		}
  	}

	await mTrade.save();
}
module.exports = mongoose.model('Trade', TradeSchema);